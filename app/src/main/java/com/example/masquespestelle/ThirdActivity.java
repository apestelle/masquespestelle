package com.example.masquespestelle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static com.example.masquespestelle.MainActivity.STREET_NUMBER;
import static com.example.masquespestelle.MainActivity.STREET_NAME;
import static com.example.masquespestelle.MainActivity.POSTAL_CODE;
import static com.example.masquespestelle.MainActivity.TOWN;


public class ThirdActivity extends AppCompatActivity {
    TextView streetNumber;
    TextView streetName;
    TextView streetCP;
    TextView town;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        streetNumber = findViewById(R.id.streetNumber);
        streetName = findViewById(R.id.streetName);
        streetCP = findViewById(R.id.postalCode);
        town = findViewById(R.id.townField);
    }


    public void onValidate(View view) {
        Intent returnIntent = new Intent();

        //récupération des nouvelles données
        returnIntent.putExtra(STREET_NUMBER, streetNumber.getText().toString());
        returnIntent.putExtra(STREET_NAME, streetName.getText().toString());
        returnIntent.putExtra(POSTAL_CODE, streetCP.getText().toString());
        returnIntent.putExtra(TOWN, town.getText().toString());

        //renvoie des nouvelles valeurs à l'activité principale
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    public void returnWithoutSaving(View view){
        finish();
    }
}