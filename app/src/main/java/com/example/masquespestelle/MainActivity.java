package com.example.masquespestelle;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static final int CHANGE_USER_IDENTITY = 1;
    static final int CHANGE_USER_LOCATION = 2;

    public static final String FIRST_NAME = "firstName";
    public static final String NAME = "name";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String STREET_NUMBER = "streetNumber";
    public static final String STREET_NAME = "streetName";
    public static final String POSTAL_CODE = "postalCode";
    public static final String TOWN = "town";

    TextView firstName;
    TextView name;
    TextView phoneNumber;
    TextView streetNumber;
    TextView streetName;
    TextView postalCode;
    TextView town;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //instanciation des attributs pour ne pas effectuer plusieurs fois les requêtes
        firstName = findViewById(R.id.firstNameField);
        name = findViewById(R.id.nameField);
        phoneNumber = findViewById(R.id.phoneNumberField);
        streetNumber = findViewById(R.id.streetNumber);
        streetName = findViewById(R.id.streetName);
        postalCode = findViewById(R.id.postalCode);
        town = findViewById(R.id.townField);
    }

    @SuppressLint("MissingSuperCall") //prevent error
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //traitement conditionnel en fonction du requestCode
        switch (requestCode)
        {
            case 1:
                //l'intent s'est bien passé
                if(resultCode == RESULT_OK){
                    //récupération des nouvelles données
                    String newFirstName = "prénom  : " + data.getStringExtra(FIRST_NAME);
                    String newName = "nom : " + data.getStringExtra(NAME);
                    String newPhoneNumber = "numéro de téléphone : " + data.getStringExtra(PHONE_NUMBER);

                    //affichage des nouvelles données
                    firstName.setText(newFirstName);
                    name.setText(newName);
                    phoneNumber.setText(newPhoneNumber);
                }
                break;

            case 2:
                //l'intent s'est bien passé
                if(resultCode == RESULT_OK){
                    //récupération des nouvelles données
                    String newStreetNumber = "numéro  : " + data.getStringExtra(STREET_NUMBER);
                    String newStreetName = "nom de rue : " + data.getStringExtra(STREET_NAME);
                    String newStreetCP = "code postal : " + data.getStringExtra(POSTAL_CODE);
                    String newTown = "ville : " + data.getStringExtra(TOWN);

                    //affichage des nouvelles données
                    streetNumber.setText(newStreetNumber);
                    streetName.setText(newStreetName);
                    postalCode.setText(newStreetCP);
                    town.setText(newTown);
                }
                break;
        }
    }


    //Création d'un intent destiné à la classe ThirdActivity
    public void changeAddressInformation(View view) {
        Intent intent = new Intent(this, ThirdActivity.class);
        startActivityForResult(intent, CHANGE_USER_LOCATION);
    }

    //Création d'un intent destiné à la classe SecondActivity
    public void changeUserInformation(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivityForResult(intent, CHANGE_USER_IDENTITY);
    }
}