package com.example.masquespestelle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import android.widget.TextView;

import static com.example.masquespestelle.MainActivity.FIRST_NAME;
import static com.example.masquespestelle.MainActivity.NAME;
import static com.example.masquespestelle.MainActivity.PHONE_NUMBER;

public class SecondActivity extends AppCompatActivity {
    TextView firstName;
    TextView name;
    TextView phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        firstName = findViewById(R.id.firstNameField);
        name = findViewById(R.id.nameField);
        phoneNumber = findViewById(R.id.phoneNumberField);
    }

    public void onValidate(View view) {
        Intent returnIntent = new Intent();

        //récupération des nouvelles données
        returnIntent.putExtra(FIRST_NAME, firstName.getText().toString());
        returnIntent.putExtra(NAME, name.getText().toString());
        returnIntent.putExtra(PHONE_NUMBER, phoneNumber.getText().toString());

        //renvoie des nouvelles valeurs à l'activité principale
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    public void returnWithoutSaving(View view){
        finish();
    }
}